.. _readme:

Introduction
============

Minty template project for Python (micro)services.

Getting started
---------------

n/a

More documentation
------------------

Please see the generated documentation via CI for more information about this
module and how to contribute in our online documentation. Open index.html
when you get there:
`<https://gitlab.com/minty-python/exxellence-shared-cga/-/jobs/artifacts/master/browse/tmp/docs?job=qa>`_


Contributing
------------

Please read `CONTRIBUTING.md <https://gitlab.com/minty-python/exxellence-shared-cga/blob/master/CONTRIBUTING.md>`_
for details on our code of conduct, and the process for submitting pull requests to us.

Versioning
----------

We use `SemVer <https://semver.org/>`_ for versioning. For the versions
available, see the
`tags on this repository <https://gitlab.com/minty-python/exxellence-shared-cga/tags/>`_

License
-------

Copyright (c) 2018, Minty Team and all persons listed in
`CONTRIBUTORS <https://gitlab.com/minty-python/exxellence-shared-cga-cqs/blob/master/CONTRIBUTORS.rst>`_

This project is licensed under the EUPL, v1.2. See the
`EUPL-1.2.txt <https://gitlab.com/minty-python/exxellence-shared-cga/blob/master/LICENSES/EUPL-1.2.txt>`_
file for details.

.. SPDX-FileCopyrightText: 2021 Exxellence
..
.. SPDX-License-Identifier: EUPL-1.2

