.. SPDX-FileCopyrightText: 2021 Exxellence
..
.. SPDX-License-Identifier: EUPL-1.2

Contributors
------------
\{\{cookiecutter.full_name\}\} (\{\{cookiecutter.email\}\})
