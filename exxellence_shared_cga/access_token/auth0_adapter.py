# SPDX-FileCopyrightText: 2021 Exxellence
#
# SPDX-License-Identifier: EUPL-1.2
import requests
from .redis_adapter import RedisAdapter
from jose import jwt
from loguru import logger
from time import time
from typing import Union


class Auth0Error(Exception):
    def __init__(self, reason):
        """
        Error indicating that an error occurred int the Auth0Adapter.
        """
        self.reason = reason


class Auth0Adapter:
    """
    Configurable class for retrieving a JWT access token from a Auth0.
    """

    def __init__(
        self,
        client_id: str,
        client_secret: str,
        audience: str,
        base_url: str,
        redis_host: str,
        redis_password: Union[str, None] = None,
    ):
        """
        Construct an Auth0Adapter that returns the JWT access token for the specified client id,
        client secret and audience. The specified base URL is used to dynamically derive the token
        endpoint to use. The token is cached in redis when the token is expired a new token is
        requested
        """

        self._client_id = client_id
        self._client_secret = client_secret
        self._audience = audience
        self._base_url = base_url
        self._url = self._get_token_endpoint(self._base_url)
        self._redis = RedisAdapter(redis_host, f"{client_id}:access_token", redis_password)

    def _is_token_expired(self, access_token: str) -> bool:
        claims = jwt.get_unverified_claims(access_token)
        expire_at = claims["exp"]

        return int(time()) > expire_at

    def _get_new_token_from_auth0(self) -> str:
        logger.info(f"Calling Auth0 for a new access token ({self._url}).")

        payload = {
            "client_id": self._client_id,
            "client_secret": self._client_secret,
            "audience": self._audience,
            "grant_type": "client_credentials",
        }

        response = requests.post(url=self._url, json=payload, verify=True)

        if response.status_code == 200:
            response_json = response.json()
            return response_json["access_token"]
        else:
            raise Auth0Error(
                f"Error {response.status_code} while getting token: {response.reason}"
            )

    def get_access_token(self) -> str:
        """
        Returns access token from auth0 or redis if it is still valid.
        """

        access_token: Union[str, None] = self._redis.get_access_token()

        if access_token is None or self._is_token_expired(access_token):
            access_token = self._get_new_token_from_auth0()

            logger.info("Storing new cached token in Redis.")
            self._redis.set_access_token(access_token)

        else:
            logger.info("Reusing cached access token, it is still valid.")

        return access_token

    def get_token_endpoint_url(self) -> str:
        """
        Returns the Auth0 token endpoint.
        """

        return self._url

    def get_url(self) -> str:
        """
        Returns the token endpoint URL of Auth0.
        """

        return self._url

    def _get_token_endpoint(self, base_url: str) -> str:
        """
        Returns the tone endpoint of the OpenId configuration for the specified base URL.
        """

        response: requests.Response = requests.get(
            url=f"{base_url}/.well-known/openid-configuration", verify=True
        )

        if response.status_code == 200:
            response_json = response.json()

            return response_json["token_endpoint"]
        else:
            raise Auth0Error(f"Error {response.status_code} on OIDC discovery: {response.reason}")
