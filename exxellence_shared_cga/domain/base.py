# SPDX-FileCopyrightText: 2021 Exxellence
#
# SPDX-License-Identifier: EUPL-1.2
import sqlalchemy as sa
import sqlalchemy.exc as exc
from ._get_list import get_list
from .base_class import Base
from exxellence_shared_cga.core.types import (
    FilterOptions,
    GenericId,
    IdType,
    ProcessError,
    RangeOptions,
    SearchResult,
    SortOptions,
)
from fastapi import status as http_status
from fastapi.encoders import jsonable_encoder
from pydantic import BaseModel
from sqlalchemy.orm import Session
from typing import Any, Dict, Generic, Optional, Type, TypeVar, Union

ModelType = TypeVar("ModelType", bound=Base)
CreateSchemaType = TypeVar("CreateSchemaType", bound=BaseModel)
UpdateSchemaType = TypeVar("UpdateSchemaType", bound=BaseModel)


class CRUDBase(Generic[ModelType, CreateSchemaType, UpdateSchemaType]):
    def __init__(self, model: Type[ModelType], models: Any):
        """
        CRUD object with default methods to Create, Read, Update, Delete(CRUD)

        **Parameters**

        * `model`: A SQLAlchemy model class
        * `schema`: A Pydantic model (schema) class
        """
        self.model = model
        self.models = models

    def _get_query_filter_by_id(self, db: Session, id: GenericId):
        if id.type == IdType.id:
            return sa.select(self.model).where(self.model.id == id.id)
        if id.type == IdType.sid:
            return sa.select(self.model).where(self.model.sid == id.id)
        if id.type == IdType.uuid:
            return sa.select(self.model).where(self.model.uuid == id.id)

    def get(self, db: Session, id: GenericId) -> ModelType:
        query = self._get_query_filter_by_id(db, id)
        result = db.execute(query).scalars().first()

        if not result:
            raise ProcessError(
                loc={"source": "path", "field": "id"},
                msg="Er is geen record gevonden met {id}",
                status_code=http_status.HTTP_404_NOT_FOUND,
            )

        return result

    def get_multi(
        self,
        db: Session,
        query: Optional[Any] = None,
        sort_options: Optional[SortOptions] = None,
        range_options: Optional[RangeOptions] = None,
        filter_options: Optional[FilterOptions] = None,
        return_scalars: Optional[bool] = True,
    ) -> SearchResult:
        db_query = query if query is not None else sa.select(self.model)  # type: ignore

        return get_list(
            db=db,
            model=self.model,
            models=self.models,
            query=db_query,
            sort_options=sort_options,
            range_options=range_options,
            filter_options=filter_options,
            return_scalars=return_scalars,
        )

    def create(self, db: Session, obj_in: CreateSchemaType) -> ModelType:
        obj_in_data = obj_in.dict(exclude_unset=True)
        db_obj = self.model(**obj_in_data)  # type: ignore

        try:
            db.add(db_obj)
            db.flush()  # generate uuid used in audit log
            db.commit()
        except exc.IntegrityError as e:
            raise ProcessError(
                loc={"source": "body", "field": "name"},
                msg="Record bestaat al",
                status_code=http_status.HTTP_409_CONFLICT,
            ) from e
        db.refresh(db_obj)
        return db_obj

    def update(
        self,
        db: Session,
        *,
        id: GenericId,
        obj_in: Union[UpdateSchemaType, Dict[str, Any]],
        db_obj_to_change: Optional[ModelType] = None,
    ) -> ModelType:
        db_obj_to_update: ModelType = (
            db_obj_to_change if db_obj_to_change else self.get(db=db, id=id)
        )

        obj_data = jsonable_encoder(db_obj_to_update)

        if isinstance(obj_in, dict):
            update_data = obj_in
        else:
            update_data = obj_in.dict(exclude_unset=True)
        for field in obj_data:  # type: ignore
            if field in update_data:
                setattr(db_obj_to_update, field, update_data[field])
        try:
            db.add(db_obj_to_update)
            db.commit()
        except exc.IntegrityError as e:
            raise ProcessError(
                loc={"source": "body", "field": "name"},
                msg="Record kan niet geüpdatet worden",
                status_code=http_status.HTTP_409_CONFLICT,
            ) from e
        db.refresh(db_obj_to_update)

        return db_obj_to_update

    def delete(self, db: Session, *, id: GenericId) -> Optional[ModelType]:
        obj = self.get(db, id)

        if not obj:
            raise ProcessError(
                loc={"source": "path", "field": "id"},
                msg="Record is al verwijderd",
                status_code=http_status.HTTP_409_CONFLICT,
            )

        db.delete(obj)
        db.commit()
        return obj
