# SPDX-FileCopyrightText: 2021 Exxellence
#
# SPDX-License-Identifier: EUPL-1.2
import uuid
from sqlalchemy.ext.declarative import declared_attr
from sqlalchemy.orm import as_declarative
from typing import Union


@as_declarative()
class Base:
    id: Union[str, int]
    sid: int
    uuid: uuid.UUID
    __name__: str

    # Generate __tablename__ automatically

    @declared_attr
    def __tablename__(cls) -> str:
        return cls.__name__.lower()
