# SPDX-FileCopyrightText: 2021 Exxellence
#
# SPDX-License-Identifier: EUPL-1.2

import asyncio
from exxellence_shared_cga.domain.langchain.langchain_graph import GraphManager
from exxellence_shared_cga.domain.langchain.langchain_manager import AssistantManager
from exxellence_shared_cga.domain.langchain.langchain_state import get_or_create_memory
from exxellence_shared_cga.domain.langchain.langchain_utils import to_async_iter
from langchain_core.messages import AIMessage
from langgraph.errors import GraphRecursionError


def ask_llm(
    user_prompt: str,
    system_message: str,
    meta_data: dict | None,
    tools_node,
    thread_id: str | None = "default",
    limit: int | None = 10,
) -> str:
    """
    Function to interact with the LLM. It creates the assistant manager, builds the graph,
    handles the configuration, and returns the LLM's response.

    Args:
    user_prompt (str): The user's prompt.
    system_message (str): The system message template.
    meta_data (dict): Metadata about the user to format in the system message.
    tools_node (list): A list of tools available to the assistant.
    thread_id (str]: The unique thread ID for the conversation.

    Returns:
    str: The assistant's response.
    """

    assistant_manager = AssistantManager(default_tools=tools_node)
    assistant_runnable = assistant_manager.create_assistant(system_message, user_info=meta_data)

    memory = get_or_create_memory(thread_id)

    graph_manager = GraphManager()
    graph = graph_manager.build_graph(assistant_runnable, tools_node, memory)

    config = {
        "configurable": {
            "thread_id": thread_id,
        },
        "recursion_limit": limit,
    }

    try:
        response = graph.invoke({"messages": ("user", user_prompt)}, config)  # type: ignore
    except GraphRecursionError:
        return "Max recursion limit reached."

    message = response.get("messages")

    if isinstance(message, list):
        message = message[-1]
        return message.content

    return message.content  # type: ignore


async def ask_llm_stream(
    question: str,
    system_message: str,
    meta_data: dict | None,
    tools_node,
    thread_id: str | None,
):
    """
    Asynchronous function to interact with the LLM using streaming.
    It creates the assistant manager, builds the graph, handles the configuration,
    and streams the assistant's responses.

    Args:
    question (str): The user's question.
    system_message (str): The system message template.
    meta_data (dict): Metadata about the user to format in the system message.
    tools_node (list): A list of tools available to the assistant.
    thread_id (str]: The unique thread ID for the conversation.

    Returns:
    async generator: The streamed assistant's response.
    """

    assistant_manager = AssistantManager(default_tools=tools_node)
    assistant_runnable = assistant_manager.create_assistant(system_message, user_info=meta_data)

    memory = get_or_create_memory(thread_id)

    graph_manager = GraphManager()
    graph = graph_manager.build_graph(assistant_runnable, tools_node, memory)

    config = {
        "configurable": {
            "thread_id": thread_id or "default",
        }
    }

    events = to_async_iter(
        graph.stream({"messages": ("user", question)}, config, stream_mode="values")  # type: ignore
    )

    async for event in events:
        if "messages" in event and isinstance(event["messages"], list):
            last_message = event["messages"][-1]

            if isinstance(last_message, AIMessage):
                if isinstance(last_message.content, str):
                    yield last_message.content + "\n"
                    await asyncio.sleep(0)

                elif isinstance(last_message.content, list):
                    for tool in last_message.content:
                        if isinstance(tool, dict):
                            if "name" in tool:
                                yield f"Tool response: {tool['name']} is processing.\n"
                            elif "text" in tool:
                                yield f"{tool['text']}\n"

                        yield "Unknown response\n"
                        await asyncio.sleep(0)
