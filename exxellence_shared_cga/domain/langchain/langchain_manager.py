# SPDX-FileCopyrightText: 2021 Exxellence
#
# SPDX-License-Identifier: EUPL-1.2

import os
from datetime import datetime
from exxellence_shared_cga.domain.langchain.langchain_utils import (
    dict_to_formatted_string,
)
from langchain_aws import ChatBedrock
from langchain_core.prompts import ChatPromptTemplate

model_id = os.getenv("BEDROCK_MODEL_ID", "anthropic.claude-3-5-sonnet-20240620-v1:0")
region_name = os.getenv("BEDROCK_REGION_NAME", "eu-central-1")

llm = ChatBedrock(
    model=model_id,
    model_kwargs=dict(temperature=0.3),
    region=region_name,
)


class AssistantManager:
    def __init__(self, default_tools):
        self.default_tools = default_tools

    def create_prompt_template(self, system_message: str, user_info: dict | None):
        """
        Create a prompt template with the system message and user info.
        Args:
            system_message: The system message template.
            user_info: The user info to format in the system message.
        Returns:
            ChatPromptTemplate: The prompt template.
        """
        return ChatPromptTemplate.from_messages(
            [
                (
                    "system",
                    system_message.format(user_info=dict_to_formatted_string(user_info)),
                ),
                ("placeholder", "{messages}"),
            ]
        ).partial(time=datetime.now())

    def create_assistant(self, system_message: str, user_info: dict | None = None):
        """
        Create an assistant with the system message and user info.
        Args:
            system_message: The system message template.
            user_info: The user info to format in the system message.
        Returns:
            dict: The assistant runnable.
        """
        prompt_template = self.create_prompt_template(system_message, user_info)
        return prompt_template | llm.bind_tools(self.default_tools)
