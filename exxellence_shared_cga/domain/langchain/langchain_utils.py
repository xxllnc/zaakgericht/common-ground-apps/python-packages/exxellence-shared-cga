# SPDX-FileCopyrightText: 2021 Exxellence
#
# SPDX-License-Identifier: EUPL-1.2

import asyncio
from langchain_core.messages import ToolMessage
from langchain_core.runnables import RunnableLambda
from langgraph.prebuilt import ToolNode


def handle_tool_error(state) -> dict:
    """
    Handle errors in the tool node
    Args:
        state: The current state of the conversation.
    Returns:
        dict: The error message.
    """
    error = state.get("error")
    tool_calls = state["messages"][-1].tool_calls
    return {
        "messages": [
            ToolMessage(
                content=f"Error: {repr(error)}\n please fix your mistakes.",
                tool_call_id=tc["id"],
            )
            for tc in tool_calls
        ]
    }


def create_tool_node_with_fallback(tools: list) -> dict:
    """
    Create a tool node with fallbacks that handles errors.
    Args:
        tools: The list of tools to be used.
    Returns:
        dict: The tool node with fallback
    """
    return ToolNode(tools).with_fallbacks(
        [RunnableLambda(handle_tool_error)], exception_key="error"
    )  # type: ignore


def dict_to_formatted_string(data: dict | None) -> str:
    """
    Convert a dictionary to a formatted string.
    Args:
        data: The dictionary to be converted.
    Returns:
        str: The formatted string.
    """
    return "\n".join([f"{key}: {value}" for key, value in data.items()]) if data else ""


async def to_async_iter(iterator):
    """Convert a regular iterator into an async iterator."""
    for item in iterator:
        yield item
        await asyncio.sleep(0)
