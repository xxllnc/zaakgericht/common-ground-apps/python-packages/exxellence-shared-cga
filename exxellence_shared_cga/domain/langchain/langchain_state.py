# SPDX-FileCopyrightText: 2021 Exxellence
#
# SPDX-License-Identifier: EUPL-1.2

import sys
import threading
import time
from langgraph.checkpoint.memory import MemorySaver
from langgraph.graph.message import AnyMessage, add_messages
from typing import Annotated
from typing_extensions import TypedDict

memory_store = {}  # type: ignore


def get_or_create_memory(thread_id: str | None) -> MemorySaver:
    """
    Fetches the memory for a given thread_id. If it doesn't exist, creates a new memory.
    Updates the timestamp on access.

    Args:
    thread_id (str): The unique identifier for the conversation thread.

    Returns:
    MemorySaver: The memory instance for the thread.
    """
    current_time = time.time()

    if thread_id in memory_store:
        memory_data = memory_store[thread_id]
        memory_data["last_access"] = current_time
        memory = memory_data["memory"]
    else:
        memory = MemorySaver()
        if thread_id is not None:
            memory_store[thread_id] = {"memory": memory, "last_access": current_time}

    return memory


def cleanup_memory_store(expiration_seconds: int = 7 * 24 * 60 * 60):
    """
    Cleans up memory store by removing entries that haven't been accessed in a while.

    Args:
    expiration_seconds (int): The duration in seconds after which the memory is considered old.
    """
    current_time = time.time()
    expired_keys = [
        thread_id
        for thread_id, memory_data in memory_store.items()
        if current_time - memory_data["last_access"] > expiration_seconds
    ]

    for thread_id in expired_keys:
        del memory_store[thread_id]

    print(f"Cleaned up {len(expired_keys)} old conversations.")


def schedule_cleanup(interval_seconds: int = 24 * 60 * 60):
    """
    Schedules the cleanup_memory_store function to run at regular intervals.

    Args:
    interval_seconds (int): How often to run the cleanup in seconds.
    """

    if "pytest" in sys.modules:
        print("Skipping cleanup scheduling during tests.")
        return

    cleanup_memory_store()
    threading.Timer(interval_seconds, schedule_cleanup, [interval_seconds]).start()


class State(TypedDict):
    messages: Annotated[list[AnyMessage], add_messages]


schedule_cleanup()
