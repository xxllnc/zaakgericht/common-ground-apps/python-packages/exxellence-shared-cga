# SPDX-FileCopyrightText: 2021 Exxellence
#
# SPDX-License-Identifier: EUPL-1.2

from exxellence_shared_cga.domain.langchain.langchain_agent import Assistant
from exxellence_shared_cga.domain.langchain.langchain_state import State
from exxellence_shared_cga.domain.langchain.langchain_utils import (
    create_tool_node_with_fallback,
)
from langgraph.graph import START, StateGraph
from langgraph.prebuilt import tools_condition


class GraphManager:
    def __init__(self):
        self.builder = StateGraph(State)

    def build_graph(self, assistant_runnable, tools_node, memory):
        """
        Build the graph for the assistant.
        Args:
            assistant_runnable: The assistant runnable.
            tools_node: The list of tools available to the assistant.
            memory: The memory of the conversation.
        Returns:
            StateGraph: The state graph.
        """
        self.builder.add_node("assistant", Assistant(assistant_runnable))  # type: ignore
        self.builder.add_node("tools", create_tool_node_with_fallback(tools_node))
        self.builder.add_edge(START, "assistant")
        self.builder.add_conditional_edges(
            "assistant",
            tools_condition,
        )
        self.builder.add_edge("tools", "assistant")

        return self.builder.compile(checkpointer=memory)
