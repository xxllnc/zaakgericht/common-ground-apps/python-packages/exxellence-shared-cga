# SPDX-FileCopyrightText: 2021 Exxellence
#
# SPDX-License-Identifier: EUPL-1.2
from datetime import datetime, timezone
from pydantic import BaseModel, Field, field_serializer, field_validator
from typing import Optional
from uuid import UUID


class Source(BaseModel):
    appId: UUID = Field(..., title="Identifier of the app this event is coming from.")
    organizationId: Optional[UUID] = Field(
        None,
        title="Identifier of the organization this event is coming from, in case this is a "
        + "multitenant instance.",
    )
    userId: Optional[str] = Field(
        None,
        title="Identifier of the logged in user that triggered the event, absent/empty if the "
        + "system triggered the event.",
    )
    appInstanceId: Optional[UUID] = Field(
        None, title="Identifier of the app instance this event is coming from."
    )
    displayName: str = Field(
        ...,
        title="Human readable name of the logged in user that triggered the event. If the system "
        + "triggered the event, the name of the system.",
    )
    requestId: Optional[str] = Field(
        None, title="The (unique) id of the request that initiated this event."
    )

    @field_serializer("appId", "organizationId", "appInstanceId")
    def serialize_dt(self, uuid: UUID, _info):
        if uuid:
            return str(uuid)
        else:
            return None


class EventInfo(BaseModel):
    object: str = Field(..., title="The object concerning the event.")
    action: str = Field(..., title="The action concerning the event.")

    def get_change_type(self) -> str:
        try:
            return {"Created": "insert", "Updated": "update", "Deleted": "delete"}[self.action]
        except KeyError:
            raise ValueError(f"Unsupported value: {self.action}")


class Event(BaseModel):
    version: int = Field(1, title="Version of the message format.")
    timestamp: datetime = Field(
        None,
        title="Date and time of the event in UTC.",
        description="Will get a default value with the current datetime upon creation of an "
        + "instance. So does not need to be set manually.",
    )
    source: Source
    event: EventInfo
    resource: str = Field(..., title="Url to retrieve (GET) the resource concerning the event.")
    resourceId: Optional[str] = Field(
        None,
        title="Identifier of the resource concerning the event. Can be used to later find events "
        "for a specific resource, mainly for auditing purposes.",
    )
    content: Optional[dict] = Field(None, title="Application specific content of the event.")

    @field_serializer("timestamp")
    def serialize_datetime(self, timestamp: datetime, _info):
        return timestamp.astimezone(timezone.utc).isoformat(sep="T")

    @field_validator("timestamp", mode="before")
    def set_timestamp(cls, v):
        return v or datetime.now(timezone.utc)
