# SPDX-FileCopyrightText: 2021 Exxellence
#
# SPDX-License-Identifier: EUPL-1.2
import pytest
from botocore.exceptions import ClientError, EndpointConnectionError
from datetime import datetime
from exxellence_shared_cga import Event, EventInfo, Source, events
from unittest.mock import MagicMock, patch
from uuid import UUID

_listener_config = {
    "endpoint_url": "http://localstack:4566",
    "region_name": "eu-central-1",
    "aws_secret_access_key": "test",
    "aws_access_key_id": "test",
    "sqs_queue_name": "queue1",
}

_dispatcher_config = {
    "endpoint_url": "http://localstack:4566",
    "region_name": "eu-central-1",
    "aws_secret_access_key": "test",
    "aws_access_key_id": "test",
    "event_topic_arn": "arn:aws:sns:eu-central-1:000000000000:my-topic",
}


def test_create_event():
    event = _event()

    assert event is not None
    assert event.version == 1
    assert event.timestamp is not None


def test_fire():
    with patch("exxellence_shared_cga.events.events.boto3") as boto:
        client = MagicMock()
        boto.client.return_value = client

        client.publish.return_value = {"MessageId": "123"}

        dispatcher = events.EventDispatcher(_dispatcher_config)
        message_id = dispatcher.fire(_event())

        assert message_id == "123"


def test_get_queue_client_error():
    with patch("exxellence_shared_cga.events.events.boto3") as boto:
        client = MagicMock()
        boto.resource.return_value = client

        exception = ClientError(
            {"Error": {"Code": "AwsError", "Message": "TheMessage"}}, "operation"
        )
        client.get_queue_by_name.side_effect = exception

        with pytest.raises(events.ServiceException):
            listener = events.EventListener(_listener_config)
            listener._get_queue(_listener_config)


def test_get_queue_connection_error():
    with patch("exxellence_shared_cga.events.boto3") as boto:
        client = MagicMock()
        boto.resource.return_value = client

        exception = EndpointConnectionError(endpoint_url="wrong-endpoint")
        client.get_queue_by_name.side_effect = exception

        with pytest.raises(events.ServiceException):
            listener = events.EventListener(_listener_config)
            listener._get_queue(_listener_config)


def test_fire_event_client_error():
    sns = MagicMock()
    exception = ClientError({"Error": {"Code": "AwsError", "Message": "TheMessage"}}, "operation")
    sns.publish.side_effect = exception

    with pytest.raises(events.ServiceException):
        dispatcher = events.EventDispatcher(_dispatcher_config)
        dispatcher._publish_event(sns, _event())


def test_fire_event_connection_error():
    sns = MagicMock()
    exception = EndpointConnectionError(endpoint_url="wrong-endpoint")
    sns.publish.side_effect = exception

    with pytest.raises(events.ServiceException):
        dispatcher = events.EventDispatcher(_dispatcher_config)
        dispatcher._publish_event(sns, _event())


def test_decorator():
    handled: bool = False

    def handler(e: Event):
        nonlocal handled
        handled = True

    decorator = events.event_handler(object="Organization", action="Added")
    decorator(handler)

    listener = events.EventListener(_listener_config)
    event = _event()
    listener._handle_event(event)
    events.handlers.clear()

    assert handled


def test_wildcard_listener():
    handled: bool = False

    def handler(e: Event):
        nonlocal handled
        handled = True

    decorator = events.event_handler(object="*", action="*")
    decorator(handler)

    listener = events.EventListener(_listener_config)
    event = _event()
    listener._handle_event(event)
    events.handlers.clear()

    assert handled


def _event() -> Event:
    return Event(
        version=1,
        timestamp=datetime.utcnow(),
        source=Source(
            appId=UUID("c72886e8-2af1-42fc-98f2-4a697fb033a1"),
            organizationId=UUID("0aa2beca-3819-4f49-aba2-2631e0bcbfb8"),
            appInstanceId=UUID("c4c0d313-f5ed-44f8-a04b-03425ede89ac"),
            userId="auth0|60b8b386239e8b0070230c9b",
            displayName="admin@exxellence.nl",
            requestId=None,
        ),
        event=EventInfo(object="Organization", action="Added"),
        resource="user/eeb657a3-16c0-4e3b-a731-edb57ffd3be9",
        resourceId="123456",
        content=None,
    )
