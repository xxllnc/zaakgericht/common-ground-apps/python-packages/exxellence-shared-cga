# SPDX-FileCopyrightText: 2021 Exxellence
#
# SPDX-License-Identifier: EUPL-1.2
import pytest
from datetime import datetime, timezone
from exxellence_shared_cga import Event, EventInfo, Source
from exxellence_shared_cga.events.event_client import EventClient
from uuid import UUID


@pytest.fixture(params=["asyncio"])
def anyio_backend(request):
    return request.param


def _event() -> Event:
    return Event(
        version=1,
        timestamp=datetime.now(timezone.utc),
        source=Source(
            appId=UUID("c72886e8-2af1-42fc-98f2-4a697fb033a1"),
            organizationId=UUID("0aa2beca-3819-4f49-aba2-2631e0bcbfb8"),
            appInstanceId=UUID("c4c0d313-f5ed-44f8-a04b-03425ede89ac"),
            userId="auth0|60b8b386239e8b0070230c9b",
            displayName="admin@exxellence.nl",
            requestId=None,
        ),
        event=EventInfo(object="Organization", action="Added"),
        resource="user/eeb657a3-16c0-4e3b-a731-edb57ffd3be9",
        resourceId="eeb657a3-16c0-4e3b-a731-edb57ffd3be9",
        content=None,
    )


@pytest.mark.anyio
async def test_create_event_client(httpx_mock):
    httpx_mock.add_response()
    event_client = EventClient("https://url-to-event-api.org")

    await event_client.post_event(_event(), "jwt_token")
