# SPDX-FileCopyrightText: 2021 Exxellence
#
# SPDX-License-Identifier: EUPL-1.2
import pytest
import uuid
from datetime import datetime
from exxellence_shared_cga import Event, EventInfo, Source

DATE = "2017-02-14T09:51:46.000-0600"


def test_get_change_type_created_insert():
    info = EventInfo(object="App", action="Created")
    change_type = info.get_change_type()

    assert change_type == "insert"


def test_get_change_type_created_update():
    info = EventInfo(object="App", action="Updated")
    change_type = info.get_change_type()

    assert change_type == "update"


def test_get_change_type_created_delete():
    info = EventInfo(object="App", action="Deleted")
    change_type = info.get_change_type()

    assert change_type == "delete"


def test_get_change_type_foo():
    with pytest.raises(ValueError):
        info = EventInfo(object="App", action="foo")
        info.get_change_type()


def test_event_serialization():
    """
    Verify that all datetime fields in the Event object are serialized as ISO strings.
    """

    event = Event(
        version=1,
        timestamp=datetime.strptime(DATE, "%Y-%m-%dT%H:%M:%S.%f%z"),
        source=Source(
            appId=uuid.uuid4(),
            organizationId=None,
            userId=None,
            appInstanceId=None,
            displayName="Test",
            requestId=None,
        ),
        event=EventInfo(object="object", action="action"),
        resource="resource",
        resourceId="resourceId",
        content={},
    )

    event_json = event.model_dump()

    assert type(event_json["version"]) is int
    assert type(event_json["timestamp"]) is str
    assert type(event_json["resource"]) is str
    assert type(event_json["resourceId"]) is str


def test_source_serialization():
    """
    Verify that all UUID fields in the Source object are serialized as strings.
    """

    source = Source(
        appId=uuid.uuid4(),
        organizationId=uuid.uuid4(),
        userId=str(uuid.uuid4()),
        appInstanceId=uuid.uuid4(),
        displayName="Test",
        requestId=str(uuid.uuid4()),
    )

    source_json = source.model_dump()

    assert type(source_json["appId"]) is str
    assert type(source_json["organizationId"]) is str
    assert type(source_json["userId"]) is str
    assert type(source_json["appInstanceId"]) is str
    assert type(source_json["displayName"]) is str
    assert type(source_json["requestId"]) is str


def test_source_serialization_without_optional_fields():
    """
    Verify that all UUID fields in the Source object are serialized as strings.
    """

    source = Source(
        appId=uuid.uuid4(),
        organizationId=None,
        userId=None,
        appInstanceId=None,
        displayName="Test",
        requestId=None,
    )

    source_json = source.model_dump()

    assert type(source_json["appId"]) is str
    assert source_json["organizationId"] is None
    assert source_json["userId"] is None
    assert source_json["appInstanceId"] is None
    assert type(source_json["displayName"]) is str
    assert source_json["requestId"] is None
