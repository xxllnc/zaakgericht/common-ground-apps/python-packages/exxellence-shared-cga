# SPDX-FileCopyrightText: 2021 Exxellence
#
# SPDX-License-Identifier: EUPL-1.2

import enum
import os
import uuid
from fastapi import HTTPException
from pydantic import BaseModel, Field, model_validator
from typing import Any, Dict, List, Optional, Union
from typing_extensions import Annotated

MAX_PAGE_SIZE = int(os.environ.get("MAX_PAGE_SIZE", "100"))

# pydantic's built-in 'PositiveInt' needs a value > 0; but our use-case requires a constraint >= 0.
UnsignedInt = Annotated[int, Field(ge=0)]


class IdType(str, enum.Enum):
    id = "ID"
    uuid = "UUID"
    sid = "SID"


class SortOrder(str, enum.Enum):
    asc = "ASC"
    desc = "DESC"


class GenericId(BaseModel):
    id: Union[uuid.UUID, str, int]
    type: IdType


class FilterOptions(BaseModel):
    options: Dict


class RangeOptions(BaseModel):
    check_bounds: bool = True
    start: UnsignedInt
    end: UnsignedInt

    @model_validator(mode="after")
    def _validate_end_after_start(self):
        """
        Validates the specified start and end values:

        1) End must not be before start
        2) If check bounds is true: the start - end range must not exceed max page size.
        """

        assert self.start <= self.end, "range end cannot be before start"

        if self.check_bounds:
            assert (
                self.end - self.start
            ) <= MAX_PAGE_SIZE, f"page size cannot exceed {MAX_PAGE_SIZE}"

        return self


class SortOptions(BaseModel):
    field: str
    order: SortOrder
    modelName: Optional[str] = None


class SearchResult(BaseModel):
    result: Any = None
    start: UnsignedInt
    end: Optional[UnsignedInt] = None
    total: UnsignedInt

    def as_content_range(self, unit: str):
        """
        Convert the search result's start/end and total to a valid Content-Range. If there is no
        end (which happens if there are no results), the range will be '*' (which means "unknown").

        Reference: https://tools.ietf.org/html/rfc7233#section-4.2
        """
        if self.end is None:
            rng = "*"
        else:
            rng = f"{self.start}-{self.end}"

        return f"{unit} {rng}/{self.total}"


class SemanticErrorType(str, enum.Enum):
    already_deleted = "semantic_error.already_deleted"
    invalid_input = "semantic_error.invalid_input"
    not_found = "semantic_error.not_found"
    not_unique = "semantic_error.not_unique"
    conflict = "semantic_error.conflict"


class SemanticError(Exception):
    """
    Exception raised for semantic errors
    """

    def __init__(self, loc: dict, msg: Union[str, List[Dict[str, Any]]], type: SemanticErrorType):
        self.loc = loc
        self.msg = msg
        self.type = type
        super().__init__(self.msg)

    def detail(self):
        return {"loc": self.loc, "msg": self.msg, "type": self.type}


class ProcessError(HTTPException):
    def __init__(self, loc: dict, msg: Union[str, List[Dict[str, Any]]], status_code: int):
        self.loc = loc
        self.msg = msg
        super().__init__(status_code)

    def detail(self):
        return {"loc": self.loc, "msg": self.msg}
